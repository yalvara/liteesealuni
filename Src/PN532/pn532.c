/*
 * pn532.c
 *
 *  Created on: Nov 4, 2016
 *      Author: gis
 */
#include "../jismDiag.h"
#include "pn532.h"
#include "usart.h"
#include "string.h"

uint8_t 	pn532_packetbuffer[PN532_PACKBUFFSIZ];

#define PN532_DEBUG	1

uint8_t txData[100];
uint8_t rxData[100];
uint8_t command;
uint8_t isPN532Wakeup = 1;

// For usart2 Queue
extern uint8_t uart2Buf[100];
extern uint8_t uart2BufRear;
extern uint8_t uart2BufFront;
extern uint8_t uart2BufCount;

// For Receive Interrupt
extern uint8_t	uart2RxData[2];

PN532_StatusTypeDef PN532Status = PN532_STATUS_SLEEP;

void PN532Begin()
{
	printf("PN532 Begin ....\r\n");
	isPN532Wakeup = 1;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);

	HAL_UART_Receive_IT(&huart2, uart2RxData, 1);
}

void PN532End()
{
	printf("PN532 End ....\r\n");
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
}

PN532_StatusTypeDef GetPN532Status() {
	return PN532Status;
}

JISM_StatusTypeDef SendPN532(uint8_t *data, int8_t len) {
	int result = JISM_OK;
	HAL_StatusTypeDef hres = HAL_UART_Transmit_IT(&huart2, data, len);

	switch(hres) {
	case HAL_OK: result = JISM_OK; break;
	case HAL_TIMEOUT: result = JISM_TIMEOUT; break;
	default: result = JISM_ERROR;
	}

	return result;
}

JISM_StatusTypeDef ReadPN532(uint8_t *data, uint8_t len, uint32_t timeout) {
	uint32_t tickstart = 0x00U;
	tickstart = HAL_GetTick();

	while(uart2BufCount < len) {
		if((timeout == 0U)||((HAL_GetTick() - tickstart ) > timeout)) {
			return JISM_TIMEOUT;
		}
	}

	for(int i = 0; i < len; i++) {
		data[i] = uart2Buf[uart2BufFront];
		uart2BufFront = (uart2BufFront+1)%100;
		uart2BufCount--;
	}

	return JISM_OK;
}

void FlushPN532() {
	uart2BufFront = uart2BufRear;
	uart2BufCount = 0;
}
// PN532 communication .....................................................
int8_t readAckFrame()
{
    const uint8_t PN532_ACK[] = {0, 0, 0xFF, 0, 0xFF, 0};
    uint8_t ackBuf[6];

#if PN532_DEBUG
    printf("[PN532] Ack: ");
#endif

    if (ReadPN532(ackBuf, 6, PN532_ACK_WAIT_TIME) != JISM_OK) {
    	printf("Timeout\r\n");
    	return PN532_TIMEOUT;
    }

    if( memcmp(ackBuf, PN532_ACK, sizeof(PN532_ACK)) ){
        printf("Invalid\r\n");
        return PN532_INVALID_ACK;
    }
    printf("OK\r\n");
    return 0;
}

int8_t writeCommand(uint8_t *header, uint8_t hlen, uint8_t *body, uint8_t blen)
{
	uint8_t idx = 0;

	// TODO: FlushPN532를 해야하는가???

	command = header[0];

    if (isPN532Wakeup) {
    	printf("[PN532] PN532 Wakeup\r\n");
    	txData[idx++] = 0x55;
    	txData[idx++] = 0x55;
    	txData[idx++] = 0;
    	txData[idx++] = 0;
    	txData[idx++] = 0;
    	isPN532Wakeup = 0;
    }

    txData[idx++] = (PN532_PREAMBLE);
    txData[idx++] = (PN532_STARTCODE1);
    txData[idx++] = (PN532_STARTCODE2);

    uint8_t length = hlen + blen + 1;   // length of data field: TFI + DATA
    txData[idx++] = (length);
    txData[idx++] = (~length + 1);         // checksum of length

    txData[idx++] = (PN532_HOSTTOPN532);
    uint8_t sum = PN532_HOSTTOPN532;    // sum of TFI + DATA

    printf("[PN532] Write: ");

    for (uint8_t i = 0; i < hlen; i++) {
    	txData[idx++] = header[i];
        sum += header[i];
        DMSG_HEX(header[i]);
    }

    if ( blen > 0) {
		for (uint8_t i = 0; i < blen; i++) {
			txData[idx++] = body[i];
			sum += body[i];
			DMSG_HEX(body[i]);
		}
    }

    uint8_t checksum = ~sum + 1;            // checksum of TFI + DATA
    txData[idx++] = (checksum);
    txData[idx++] = (PN532_POSTAMBLE);
    printf("OK\r\n");
    SendPN532(txData, idx);

    return readAckFrame();
}

int16_t readResponse(uint8_t buf[], uint8_t len, uint16_t timeout)
{
    uint8_t tmp[3];

    printf("[PN532] Read:  ");

    /** Frame Preamble and Start Code */
    if (ReadPN532(tmp, 3, timeout) != JISM_OK) {
		printf("Timeout 1\r\n");
		return PN532_TIMEOUT;
	}
    if(0 != tmp[0] || 0!= tmp[1] || 0xFF != tmp[2]){
        printf("Preamble error\r\n");
        return PN532_INVALID_FRAME;
    }

    /** receive length and check */
    uint8_t length[2];
    if (ReadPN532(length, 2, timeout) != JISM_OK) {
		printf("Timeout 2\r\n");
		return PN532_TIMEOUT;
	}
    if( 0 != (uint8_t)(length[0] + length[1]) ){
        printf("Length error\r\n");
        return PN532_INVALID_FRAME;
    }
    length[0] -= 2;
    if( length[0] > len){
    	printf("NO Space\r\n");
        return PN532_NO_SPACE;
    }

    /** receive command byte */
    uint8_t cmd = command + 1;               // response command
    if (ReadPN532(tmp, 2, timeout) != JISM_OK) {
    	printf("Timeout 3\r\n");
		return PN532_TIMEOUT;
	}
    if( PN532_PN532TOHOST != tmp[0] || cmd != tmp[1]){
        printf("Command error 0x%02x, 0x%02x\r\n", tmp[0], tmp[1]);
        return PN532_INVALID_FRAME;
    }

    //if (HAL_UART_Receive(&huart2, buf, length[0], timeout) != HAL_OK) {
    if (ReadPN532(buf, length[0], timeout) != JISM_OK) {
		return PN532_TIMEOUT;
	}
    uint8_t sum = PN532_PN532TOHOST + cmd;
    for(uint8_t i=0; i<length[0]; i++){
        sum += buf[i];
        DMSG_HEX(buf[i]);
    }
    //printf("D: 0x%02x, 0x%02x\r\n", length[0], sum);

    /** checksum and postamble */
    if (ReadPN532(tmp, 2, timeout) != JISM_OK) {
		return PN532_TIMEOUT;
	}
    if( 0 != (uint8_t)(sum + tmp[0]) || 0 != tmp[1] ){
        printf("Checksum error 0x%02x, 0x%02x\r\n", tmp[0], tmp[1]);
        return PN532_INVALID_FRAME;
    }
    printf("OK\r\n");

    return length[0];
}


// Functions...................................................
uint32_t getFirmwareVersion(void)
{
    uint32_t response;

    pn532_packetbuffer[0] = PN532_COMMAND_GETFIRMWAREVERSION;

    if (writeCommand(pn532_packetbuffer, 1, 0, 0)) {
    	return 0;
    }

    // read data packet
    int16_t status = readResponse(pn532_packetbuffer, sizeof(pn532_packetbuffer), 1000);
    if (0 > status) {
        return 0;
    }

    response = pn532_packetbuffer[0];
    response <<= 8;
    response |= pn532_packetbuffer[1];
    response <<= 8;
    response |= pn532_packetbuffer[2];
    response <<= 8;
    response |= pn532_packetbuffer[3];

    return response;
}

uint8_t SAMConfig(void)
{
    pn532_packetbuffer[0] = PN532_COMMAND_SAMCONFIGURATION;
    pn532_packetbuffer[1] = 0x01; // normal mode;
    pn532_packetbuffer[2] = 0x00;
    //pn532_packetbuffer[2] = 0x14; // timeout 50ms * 20 = 1 second
    pn532_packetbuffer[3] = 0x01; // use IRQ pin!

    printf("SAMConfig\r\n");

    if (writeCommand(pn532_packetbuffer, 2, 0, 0) < 0)
        return JISM_ERROR;

    return readResponse(pn532_packetbuffer, sizeof(pn532_packetbuffer), 1000);
}

uint8_t readPassiveTargetID(uint8_t cardbaudrate, uint8_t *uid, uint8_t *uidLength, uint16_t timeout)
{
    pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
    pn532_packetbuffer[1] = 1;  // max 1 cards at once (we can set this to 2 later)
    pn532_packetbuffer[2] = cardbaudrate;

    if (writeCommand(pn532_packetbuffer, 3, 0, 0))
    	return 0;

    // read data packet
    if (readResponse(pn532_packetbuffer, sizeof(pn532_packetbuffer), timeout) < 0) {
    	return 0;
    }

    // check some basic stuff
    /* ISO14443A card response should be in the following format:
      byte            Description
      -------------   ------------------------------------------
      b0              Tags Found
      b1              Tag Number (only one used in this example)
      b2..3           SENS_RES
      b4              SEL_RES
      b5              NFCID Length
      b6..NFCIDLen    NFCID
    */

    if (pn532_packetbuffer[0] != 1)
        return 0;

    uint16_t sens_res = pn532_packetbuffer[2];
    sens_res <<= 8;
    sens_res |= pn532_packetbuffer[3];

    DMSG("ATQA: 0x");  DMSG_HEX(sens_res);
    DMSG("SAK: 0x");  DMSG_HEX(pn532_packetbuffer[4]);
    DMSG("\r\n");

    /* Card appears to be Mifare Classic */
    *uidLength = pn532_packetbuffer[5];

    for (uint8_t i = 0; i < pn532_packetbuffer[5]; i++) {
        uid[i] = pn532_packetbuffer[6 + i];
    }

    return 1;
}
