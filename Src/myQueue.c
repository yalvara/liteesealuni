/*
 * myQueue->c
 *
 *  Created on: Nov 7, 2016
 *      Author: gis
 */

#include "myQueue.h"

void initQueue(Queue *q, UART_HandleTypeDef *handle) {
	q->handle = handle;
	q->front = 0;
	q->rear = 0;
	q->count = 0;
}

void clearQueue (Queue *q) {
   q->front = q->rear;
   q->count = 0;
}

uint8_t enQueue(Queue *q, uint8_t val) {
    if ((q->rear+1) % QUEUE_MAXSIZE == q->front) {    // 큐가 꽉 찼는지 확인
        printf ("Queue Overflow->");
        return -1;
    }

    q->data[q->rear] = val;                    // rear가 큐의 끝 다음의 빈공간이므로 바로 저장
    q->rear = (q->rear + 1) % QUEUE_MAXSIZE;             // rear를 다음 빈공간으로 변경
    q->count++;
    //printf("enqueue %d\r\n", q->count);
    return val;
}

uint8_t deQueue (Queue *q) {
    uint8_t i;

    if (q->front == q->rear) {                  // 큐가 비어 있는지 확인
        printf ("    Queue Underflow->");
        return (-1);
    }

    i = q->data[q->front];    // front의 값을 가져옴
    q->front = (q->front + 1) % QUEUE_MAXSIZE;   // front를 다음 데이터 요소로
    q->count--;
    return i;
}
