/*
 * jismDiag.h
 *
 *  Created on: Nov 10, 2016
 *      Author: gis
 *     Version: 0.1a
 *
 *  Ji Snagmin's Diagnostic for 'C Language'
 */

#ifndef JISMDIAG_H_
#define JISMDIAG_H_

/**
  * @brief  jism Status structures definition
  *
  */
typedef enum
{
  JISM_OK       = 0x00,
  JISM_ERROR    = -1,
  JISM_BUSY     = -2,
  JISM_TIMEOUT  = -3
} JISM_StatusTypeDef;

#define JISM_TRUE		1
#define JISM_FALSE		0

#define JISM_SUCCESS	0
#define JISM_FAIL		1


// MACRO
#define DMSG(msg) printf("msg")
#define DMSG_HEX(num) printf("0x%02X ",num)


#endif /* JISMDIAG_H_ */
