/*
 * mySerial.h
 *
 *  Created on: Nov 7, 2016
 *      Author: gis
 */

#ifndef MYSERIAL_H_
#define MYSERIAL_H_

#define SERIAL_MAXNUM 1

#define Q_PN532 0

int EnqueueSerial(int kind, uint8_t data);
void ClearSerial(int kind);
int SendSerial(int kind, uint8_t *data, int8_t len);
int SerialAvailable(int kind);
int RecvSerial(int kind, uint8_t *data, uint8_t len, uint32_t timeout);

#endif /* MYSERIAL_H_ */
