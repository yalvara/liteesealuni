/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MXCONSTANT_H
#define __MXCONSTANT_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define STATUS_Pin GPIO_PIN_13
#define STATUS_GPIO_Port GPIOC
#define NFC_RST_Pin GPIO_PIN_4
#define NFC_RST_GPIO_Port GPIOA
#define NFC_IRQ_Pin GPIO_PIN_5
#define NFC_IRQ_GPIO_Port GPIOA
#define NFC_HREQ_Pin GPIO_PIN_6
#define NFC_HREQ_GPIO_Port GPIOA
#define DETECT_Pin GPIO_PIN_7
#define DETECT_GPIO_Port GPIOA
#define AXIS_INT_Pin GPIO_PIN_1
#define AXIS_INT_GPIO_Port GPIOB
#define POWER_LED_Pin GPIO_PIN_12
#define POWER_LED_GPIO_Port GPIOB
#define LED_EN_Pin GPIO_PIN_13
#define LED_EN_GPIO_Port GPIOB
#define CHARGE_Pin GPIO_PIN_14
#define CHARGE_GPIO_Port GPIOB
#define SOLENOID_EN_Pin GPIO_PIN_15
#define SOLENOID_EN_GPIO_Port GPIOB
#define BLE_INT2_Pin GPIO_PIN_8
#define BLE_INT2_GPIO_Port GPIOA
#define BLE_INT1_Pin GPIO_PIN_15
#define BLE_INT1_GPIO_Port GPIOA
#define BUZZER_O_Pin GPIO_PIN_5
#define BUZZER_O_GPIO_Port GPIOB
#define GPS_EN_Pin GPIO_PIN_7
#define GPS_EN_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MXCONSTANT_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
