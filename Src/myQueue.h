/*
 * myQueue.h
 *
 *  Created on: Nov 7, 2016
 *      Author: gis
 */

#ifndef MYQUEUE_H_
#define MYQUEUE_H_

#include "stm32l0xx_hal.h"

#define QUEUE_MAXSIZE 150

typedef struct _Queue{
	UART_HandleTypeDef *handle;
	uint8_t data[QUEUE_MAXSIZE];
	uint32_t front;
	uint32_t rear;
	uint32_t count;
} Queue;

void initQueue(Queue *q, UART_HandleTypeDef *handle);
void clearQueue (Queue *q);
uint8_t enQueue(Queue *q, uint8_t val);
uint8_t deQueue (Queue *q);



#endif /* MYQUEUE_H_ */
