/*
 * mySerial.c
 *
 *  Created on: Nov 7, 2016
 *      Author: gis
 */

#include "usart.h"
#include "myQueue.h"
#include "mySerial.h"

Queue queues[SERIAL_MAXNUM];

void InitSerial()
{
	initQueue(&queues[0], &huart2);
}

void ClearSerial(int kind) {
	Queue q = queues[kind];
	clearQueue(&q);
}

int SendSerial(int kind, uint8_t *data, int8_t len) {
	Queue q = queues[kind];
	//HAL_UART_Transmit_IT(q.handle, data, len);
	HAL_UART_Transmit_IT(&huart2, data, len);
	return HAL_OK;
}

int SerialAvailable(int kind) {
	Queue q = queues[kind];
	return q.count;
}

extern uint8_t uart2Buf[100];
extern uint8_t uart2BufRear;
extern uint8_t uart2BufFront;
extern uint8_t uart2BufCount;

int RecvSerial(int kind, uint8_t *data, uint8_t len, uint32_t timeout) {
	//Queue q = queues[kind];
	uint32_t tickstart = 0x00U;
	tickstart = HAL_GetTick();

	//while(q.count < len) {
	while(uart2BufCount < len) {
		if((timeout == 0U)||((HAL_GetTick() - tickstart ) > timeout)) {
			return HAL_TIMEOUT;
		}
	}

	for(int i = 0; i < len; i++) {
		//data[i] = deQueue(&q);
		data[i] = uart2Buf[uart2BufFront];
		uart2BufFront = (uart2BufFront+1)%100;
		uart2BufCount--;
	}

	//printf("RecvSerial OK, count = %u\r\n", q.count);

	return HAL_OK;
}

int EnqueueSerial(int kind, uint8_t data) {
	Queue q = queues[kind];
	if (enQueue(&q, data) < 0) {
		return -1;
	}
	return HAL_OK;
}

/*
int sendSerial(int kind, int8_t *data, int8_t len) {
	int i = 0;
	Queue q = queues[kind];

	for(;i < len; i++) {
		if (enQueue(&q, data[i]) < 0) {
			return -1;
		}
	}

	// Send
	HAL_UART_Transmit_IT(q.handle, uint8_t *pData, uint16_t Size)

	return 0;
}
*/
